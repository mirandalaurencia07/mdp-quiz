package edu.stts;

import android.view.View;

public interface RVClickListener {
    public void recyclerViewClick(View v, int posisi);
}
