package edu.stts;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    EditText edName;
    RadioGroup rgType;
    RadioButton rbTea, rbCoffee, rbSmoothies;
    CheckBox cbPearl, cbPudding, cbRedBean, cbCoconut;
    Button btnMinus, btnPlus, btnAdd, btnEdit, btnDelete, btnReset;
    TextView txtTotal, txtQty, txtName;
    RecyclerView rvOrder;
    ListView lvOrder;
    orderAdapter adapter;
    ArrayList<Order> arrOrder = new ArrayList<>();
    long total = 0;
    int index = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Context mContext = getApplicationContext();;
        setContentView(R.layout.activity_main);
        edName = findViewById(R.id.editText_name);
        rgType = findViewById(R.id.radioGroup_type);
        rbTea = findViewById(R.id.radiobutton_tea);
        rbCoffee = findViewById(R.id.radiobutton_coffee);
        rbSmoothies = findViewById(R.id.radiobutton_smoothies);
        cbPearl = findViewById(R.id.checkbox_pearl);
        cbRedBean = findViewById(R.id.checkbox_red_bean);
        cbPudding = findViewById(R.id.checkbox_pudding);
        cbCoconut = findViewById(R.id.checkbox_coconut);
        btnMinus = findViewById(R.id.button_minus);
        btnPlus = findViewById(R.id.button_plus);
        txtQty = findViewById(R.id.textView_qty);
        btnAdd = findViewById(R.id.button_add);
        btnDelete = findViewById(R.id.button_delete);
        btnReset = findViewById(R.id.button_reset);
        rvOrder = findViewById(R.id.recyclerview_order);

        txtName = findViewById(R.id.textView_name);
        txtTotal = findViewById(R.id.textView_total);
        adapter = new orderAdapter(arrOrder, mContext, new RVClickListener() {
            @Override
            public void recyclerViewClick(View v, int posisi) {
                Toast.makeText(MainActivity.this, "Posisi " + posisi, Toast.LENGTH_LONG).show();
                cbPearl.setChecked(false); cbRedBean.setChecked(false); cbPudding.setChecked(false); cbCoconut.setChecked(false);
                String tipe = arrOrder.get(posisi).getType().toString();
                Toast.makeText(MainActivity.this, "Tipe " + tipe, Toast.LENGTH_LONG).show();
                if (tipe.contentEquals("Tea")) rbTea.setChecked(true);
                else if (tipe.contentEquals("Coffee")) rbCoffee.setChecked(true);
                else if (tipe.contentEquals("Smoothies")) rbSmoothies.setChecked(true);

                for (int i=0; i<arrOrder.get(posisi).getToppings().size(); i++) {
                    String top = arrOrder.get(posisi).getToppings().get(i).toString();
                    if (top.contentEquals("Pearl")) cbPearl.setChecked(true);
                    if (top.contentEquals("Red Bean")) cbRedBean.setChecked(true);
                    if (top.contentEquals("Pudding")) cbPudding.setChecked(true);
                    if (top.contentEquals("Coconut")) cbCoconut.setChecked(true);
                }

                txtQty.setText(Integer.toString(arrOrder.get(posisi).getQty()));

                index = posisi;
            }
        });
        rvOrder.setAdapter(adapter);
        total = 0;

        RecyclerView.LayoutManager lm = new LinearLayoutManager(MainActivity.this);
        rvOrder.setLayoutManager(lm);
        rvOrder.setAdapter(adapter);

        btnMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int qty = Integer.parseInt(txtQty.getText().toString());
                if (qty > 1) qty -= 1;
                else qty = 1;
                txtQty.setText(Integer.toString(qty));
                Toast.makeText(getApplicationContext(), Integer.toString(qty), Toast.LENGTH_LONG).show();
            }
        });

        btnPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int qty = Integer.parseInt(txtQty.getText().toString());
                if (qty >= 1) qty += 1;
                else qty = 1;
                txtQty.setText(Integer.toString(qty));
                Toast.makeText(getApplicationContext(), Integer.toString(qty), Toast.LENGTH_LONG).show();
            }
        });

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String tampName = edName.getText().toString();
                if (!tampName.contentEquals("")) {
                    txtName.setText(tampName);

                    // Type
                    String tampType = "";
                    int harga = 0;
                    if (rbTea.isChecked()) {
                        tampType = rbTea.getText().toString();
                        harga = 23000;
                    } else if (rbCoffee.isChecked()) {
                        tampType = rbCoffee.getText().toString();
                        harga = 25000;
                    } else if (rbSmoothies.isChecked()) {
                        tampType = rbSmoothies.getText().toString();
                        harga = 30000;
                    }

                    // Topping
                    ArrayList<String> arrTop = new ArrayList<>();
                    int hargaTop = 0;
                    if (cbPearl.isChecked()) {
                        arrTop.add(cbPearl.getText().toString());
                        hargaTop += 3000;
                    }
                    if (cbRedBean.isChecked()) {
                        arrTop.add(cbRedBean.getText().toString());
                        hargaTop += 4000;
                    }
                    if (cbPudding.isChecked()) {
                        arrTop.add(cbPudding.getText().toString());
                        hargaTop += 3000;
                    }
                    if (cbCoconut.isChecked()) {
                        arrTop.add(cbCoconut.getText().toString());
                        hargaTop += 4000;
                    }

                    // Qty
                    int qty = Integer.parseInt(txtQty.getText().toString());

                    // Subtotal
                    int subTotal = qty*harga + hargaTop;
                    total += subTotal;

                    Order or = new Order(tampType, arrTop, qty, subTotal);
                    arrOrder.add(or);

                    Toast.makeText(getApplicationContext(), tampName + " " + qty + " " + subTotal + " " + arrTop, Toast.LENGTH_LONG).show();
                    adapter.notifyDataSetChanged();
                    txtTotal.setText(Long.toString(total));
                    afterAdd_Delete();
                } else {
                    Toast.makeText(getApplicationContext(), "Field Name cannot be empty", Toast.LENGTH_LONG).show();
                    reset();
                }
            }
        });

        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                reset();
                adapter.notifyDataSetChanged();
            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (index != -1) {
                    arrOrder.remove(index);
                    adapter.notifyDataSetChanged();
                    index = -1;
                    int tampTot = 0;
                    for (int i=0; i<arrOrder.size(); i++) {
                        tampTot += arrOrder.get(i).getSubtotal();
                    }
                    txtTotal.setText(Integer.toString(tampTot));
                    afterAdd_Delete();
                }
            }
        });
    }

    public void reset() {
        txtName.setText("Cust");
        edName.setText("");
        rbTea.setChecked(true);
        cbPearl.setChecked(false);
        cbRedBean.setChecked(false);
        cbPudding.setChecked(false);
        cbCoconut.setChecked(false);
        txtTotal.setText("0");
        arrOrder.clear();
    }

    public void afterAdd_Delete() {
        rbTea.setChecked(true);
        cbPearl.setChecked(false);
        cbRedBean.setChecked(false);
        cbPudding.setChecked(false);
        cbCoconut.setChecked(false);
        txtQty.setText("1");
    }
}
