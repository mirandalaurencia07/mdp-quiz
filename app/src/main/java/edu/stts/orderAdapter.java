package edu.stts;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class orderAdapter extends RecyclerView.Adapter<orderAdapter.ViewHolder> {

    private ArrayList<Order> arrOrder;
    private Context mContext;
    private static RVClickListener myListener;

    public orderAdapter(ArrayList<Order> data, Context context, RVClickListener rvCL) {
        this.arrOrder = data;
        this.mContext = context;
        this.myListener = rvCL;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View v = inflater.inflate(R.layout.row_item_order, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        // Mengisi widget dengan data dari ArrayList
        viewHolder.txtType.setText(Integer.toString(arrOrder.get(i).getQty()) + " " + arrOrder.get(i).getType());
        viewHolder.txtTopping.setText("with " + arrOrder.get(i).getToppings().toString());
        viewHolder.txtSubtotal.setText("Rp. " + arrOrder.get(i).getSubtotal());
    }

    @Override
    public int getItemCount() {
        return (arrOrder != null) ? arrOrder.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txtSubtotal, txtType, txtTopping;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            // findViewById dilakukan di constructor viewHolder
            txtType = itemView.findViewById(R.id.textView_qty_type);
            txtTopping = itemView.findViewById(R.id.textView_toppings);
            txtSubtotal = itemView.findViewById(R.id.textView_subtotal);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // getLayoutPosition() mendapat posisi yang di click atau di touch
                    myListener.recyclerViewClick(view, ViewHolder.this.getLayoutPosition());
                }
            });
        }
    }


}
